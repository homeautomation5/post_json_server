#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <SimpleDHT.h>

// select corret port. On  WeMos D1 R2 is used in this example D2
int pinDHT11 = D2;
// ip adressa of waiting server for  post JSON
String URLserver_listen_json="http://172.16.42.5:5000/postjson";
String name_array[] = {"temperatura", "vlaznost"};
String name;

SimpleDHT11 dht11;
// accuracy of DHT11 is ±2℃

void setup() {

  Serial.begin(115200);                            //Serial connection
  WiFi.begin("testing", "vili1234");   //WiFi connection

  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion

    delay(500);
    Serial.println("Waiting for connection");

  }

}

void loop() {



  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

    byte temperature = 0;
    byte humidity = 0;
    int err = SimpleDHTErrSuccess;
    if ((err = dht11.read(pinDHT11, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
      Serial.print("Read DHT11 failed, err="); Serial.println(err); delay(1000);
      return;
    }

    Serial.print("Sample OK: ");
    Serial.print((float)temperature); Serial.print(" *C, ");
    Serial.print((float)humidity); Serial.println(" H");

    // DHT11 sampling rate is 1HZ.
    delay(2500);
    float t = (float)temperature;
    float h = (float)humidity;
    float  value;
    float  value_array[] = {t, h};

    for (int i = 0; i <= 1; i++) {
      value = value_array[i];
      name = name_array[i];
      StaticJsonBuffer<300> JSONbuffer;   //Declaring static JSON buffer
      // JsonObject& JSONencoder = JSONbuffer.createObject();
      String json = "{\"" + String(i) + "\":{\"publish_type\": \"graphite\", \"name\": \"" + name + "\", \"value\":" + value + ", \"location\": \"home\", \"device\": \"dh11\", \"message\": \"We did it!\", \"unit\": \"C\"}}";
      JsonObject& JSONencoder = JSONbuffer.parseObject(json);

      char JSONmessageBuffer[300];
      JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
      Serial.println(JSONmessageBuffer);

      HTTPClient http;    //Declare object of class HTTPClient

      http.begin(URLserver_listen_json);      //Specify request destination
      http.addHeader("Content-Type", "application/json");  //Specify content-type header

      int httpCode = http.POST(JSONmessageBuffer);   //Send the request
      String payload = http.getString();                                        //Get the response payload

      Serial.println(httpCode);   //Print HTTP return code
      Serial.println(payload);    //Print request response payload
      Serial.println("-----------------");
      http.end();  //Close connection
    }
  } else {
    Serial.println("Error in WiFi connection");
  }
  Serial.println("waiting #############################");
  delay(30000);  //Send a request every 30 seconds

}

