#!/usr/bin/env python

from flask import Flask
from flask import request
import time
import socket
import json

CARBON_SERVER = 'mygraphite'
CARBON_PORT = 2003

app = Flask(__name__)


@app.route('/postjson', methods=['POST'])
def postJsonHandler():
    content = request.get_json()
    # print(content)
    parsed_json = json.loads(json.dumps(content))
    for key, keyvalue in parsed_json.items():
         value = (keyvalue["value"])
         device = (keyvalue["device"])
         name = (keyvalue["name"])
         location = (keyvalue["location"])
#        #sending
         message = str(location)+'.'+str(device)+'.'+str(name)+' '+str(value)+' %d\n' % int(time.time())
         print 'sending message:\n%s' % message
         sock = socket.socket()
         sock.connect((CARBON_SERVER, CARBON_PORT))
         sock.sendall(message)
         sock.close()

#   # writing last POST JSON to file:
    file = open("lastJson.txt", "w")
    file.write(str(content))
    file.close()
    return 'JSON posted'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
