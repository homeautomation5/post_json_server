import requests
import json

data = {"0":{"publish_type": "graphite", "name": "vlaznost", "value": 15, "location": "home", "device": "dh11", "message": "We did it!", "unit": "C"},"1":{"publish_type": "graphite", "name": "temperatura", "value": 35, "location": "home", "device": "dh11", "message": "We did it!", "unit": "C"}}

parsed_json = json.loads(json.dumps(data))

r = requests.post("http://192.168.1.3:5000/postjson", json=parsed_json)

