# server za sprejemanje JSON in pošiljanje v grafano

Na vseh napravah lahko naredimo pošiljanje - POST json-a in se ne ukvarjamo kako se bo to potem poslalo ali nastavljalo.
Tudi koda za pošiljanje je enostavnejša.

## namestitev
Za namestitev se uporabi docker. Tukaj je opisana namestitev tako grafane, graphita kot pošiljanja:
Gremo v mapo v kateri bo nameščeni podatki (data)  npr. `cd /`. Če je grafana in graphite že nameščen ju je treba samo dodati v omrežje (glej `namestitev omrežja:`) in dodati v to omrežje.


 * namestitev omrežja: 
 
       `docker network create mynet`
   
 * grafana: 
   
       `docker run --restart=always -d -p 8888:3000  --network=mynet --name=mygrafana -v $PWD/data/grafana:/var/lib/grafana:rw grafana/grafana`
    
 * graphite: 
 
       `docker run --restart=always -d -p 8085:80 -p 2003:2003 --network=mynet --name=mygraphite -v $PWD/data/graphite/whisper:/opt/graphite/storage/whisper sitespeedio/graphite`
   
 * Strežnik za sprejemanje JSON in pošiljanje: [https://hub.docker.com/r/luckol/post2graphite/](https://hub.docker.com/r/luckol/post2graphite/)
   
       `docker run --restart=always -d --network=mynet -p 5000:5000 luckol/post2graphite`

 * V [ArduinoSendJson](/ArduinoSendJson/ArduinoSendJson.ino) je treba vpisati IP od graphite strežnika (vrstica 9)



## Testiranje pošiljanja 
* preko curl (zagnan na tem računalniku na katerem je nameščen - localhost. Če ni ga je treba na koncu zamenjati z ustreznim): 

     `curl -H "Content-Type: application/json" -X POST -d '{"0":{"publish_type": "graphite", "name": "vlaznost", "value": 15, "location": "home", "device": "dh11", "message": "We did it!", "unit": "C"},"1":{"publish_type": "graphite", "name": "temperatura", "value": 25, "location": "home", "device": "dh11", "message": "We did it!", "unit": "C"}}' http://localhost:5000/postjson`

* preko skripte python: [send_temperature_humidity.py](send_temperature_humidity.py) z ukazom `python send_temperature_humidity.py`
Za pregled pošiljanja se lahko gleda tudi v [grafani - http://localhost:8888](http://localhost:8888)

## Dokumenti
* [dokumenti](/documents)
* [primer JSONA](/documents/example.json)

## @TODO
* Narediti docker cluster
* Posplošiti še za pošiljanja v druge vire (mysql, sql...)
