FROM ubuntu:16.04

RUN apt-get update && apt-get -y install python-dev python-pip  && pip install --upgrade pip && pip install Flask

ADD flask_server.py /app.py
ADD run.sh	/run.sh

CMD /run.sh
